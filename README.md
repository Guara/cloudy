# CloudyHome

## Scope

A personal cloud platform with focus on Home management and personal **encrypted** data 

### Features

- [ ] Users creation with role and permissions
- [ ] Per-user notes
- [ ] Per-user ToDo lists
- [ ] Per-user calendar
- [ ] Per-user contacts management
- [ ] Per-user file manager
- [ ] Per-user domotics devices handling
- [ ] Import data from Google/Apple/Microsoft/etc services
- [ ] Beautiful web application
- [ ] Beautiful mobile applications
- [ ] Beautiful desktop applications

### How To generate valid encryption keys
```
openssl genpkey -algorithm EC -pkeyopt ec_paramgen_curve:P-384 -pkeyopt ec_param_enc:named_curve | openssl pkcs8 -topk8 -nocrypt -outform pem > p384-private-key.p8
openssl pkey -pubout -inform pem -outform pem -in p384-private-key.p8 -out p384-public-key.spki
```
