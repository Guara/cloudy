use libreauth::pass::ErrorCode;
use thiserror::Error;
use tonic::Status;

pub type UsersServiceResult<T> = Result<T, UsersServiceError>;

#[derive(Error, Debug)]
pub enum UsersServiceError {
    #[error(transparent)]
    EnvError(#[from] dotenv::Error),
    #[error(transparent)]
    DatabaseError(#[from] sqlx::Error),
    #[error(transparent)]
    DatabaseMigrateError(#[from] sqlx::migrate::MigrateError),
    #[error(transparent)]
    TonicTransportError(#[from] tonic::transport::Error),
    #[error("Internal Server Error")]
    InternalServerError,
    #[error("User not found")]
    UserNotFound,
    #[error("Error hashing the password: {:?}", .0)]
    PasswordHashingError(libreauth::pass::ErrorCode),
}

impl Into<Status> for UsersServiceError {
    fn into(self) -> Status {
        match self {
            Self::InternalServerError => Status::internal(format!("{}", self)),
            Self::UserNotFound => Status::invalid_argument(format!("{}", self)),
            Self::EnvError(_) => Status::internal(format!("{}", self)),
            Self::DatabaseError(_) => Status::internal(format!("{}", self)),
            Self::DatabaseMigrateError(_) => Status::internal(format!("{}", self)),
            Self::TonicTransportError(_) => Status::internal(format!("{}", self)),
            Self::PasswordHashingError(_) => Status::invalid_argument(format!("{}", self)),
        }
    }
}

impl From<libreauth::pass::ErrorCode> for UsersServiceError {
    fn from(code: ErrorCode) -> Self {
        Self::PasswordHashingError(code)
    }
}
