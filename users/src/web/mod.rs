use tonic::transport::Server;
use tracing::debug;

use crate::config;
use crate::errors::UsersServiceResult;
use crate::rpcs::users_proto_types::users_server::UsersServer;
use crate::rpcs::UsersService;

/// Start the tonic main loop
#[tracing::instrument]
pub async fn start_tonic_server() -> UsersServiceResult<()> {
    // Build all the gRPC services we serve
    debug!("Building gRPC services");
    let users = UsersService::new();
    debug!("gRPC services ok");

    Server::builder()
        .add_service(UsersServer::new(users))
        .serve(*config::LISTENING_ADDRESS)
        .await?;

    Ok(())
}
