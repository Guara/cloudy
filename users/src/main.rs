#![warn(missing_docs)]
#![warn(rustdoc)]
#![warn(unused)]
#![warn(rust_2018_idioms)]
#![warn(rust_2018_compatibility)]
#![warn(future_incompatible)]
#![warn(nonstandard_style)]
#![warn(unused)]
#![forbid(unsafe_code)]

//! Provide and handle the users of the system

mod config;
mod entities;
mod errors;
mod infrastructure;
mod rpcs;
mod use_cases;
mod web;

use tracing::{debug, info};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    //init code
    config::load_env()?;
    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();

    info!("Users service is preparing to rock");

    infrastructure::repository::init_pg_pool().await?;
    debug!("Starting tonic");
    web::start_tonic_server().await?; //this actually start the main loop
    Ok(())
}
