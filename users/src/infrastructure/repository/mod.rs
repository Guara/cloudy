//! All the DB details should be here

use std::env;

use once_cell::sync::OnceCell;
use sqlx::postgres::PgPoolOptions;
use sqlx::{migrate, PgPool};
use tracing::debug;

use crate::errors::UsersServiceResult;

const POOL_MAX_CONNECTIONS: u32 = 5;
static PG_POOL: OnceCell<PgPool> = OnceCell::new();

/// Init a PostgreSQL connection pool
#[tracing::instrument]
pub async fn init_pg_pool() -> UsersServiceResult<()> {
    debug!("Initializing PostgreSQL connection pool");
    PG_POOL
        .set(
            PgPoolOptions::new()
                .max_connections(POOL_MAX_CONNECTIONS)
                .connect(postgres_db_url().as_str())
                .await?,
        )
        .expect("Fail to init postgres pool");
    debug!("PostgreSQL pool initialized");
    //Run migration
    migrate!().run(PG_POOL.get().unwrap()).await?;

    debug!("PostgreSQL pool initialized");
    Ok(())
}

/// Return a new pointer to the PostgreSQL connection pool, PgPool uses Arc internally
pub fn get_pool() -> PgPool {
    PG_POOL.get().expect("PG_POOL not initialized").clone()
}

/// Retrieve PostgreSQL connection url
fn postgres_db_url() -> String {
    env::var("DATABASE_URL").expect("PG_URL env variable must be set")
}
