use std::ops::Deref;
use std::{env, fs};

use chrono::Utc;
use once_cell::sync::Lazy;
use serde::{Deserialize, Serialize};
use tracing::debug;

use crate::use_cases::LoginUser;

const ISSUER: &str = "CloudyHome";
const TOKEN_DURATION: i64 = 3600; //an hour

static EC_PRIVATE_KEY: Lazy<String> = Lazy::new(|| {
    fs::read_to_string(env::var("EC_PRIVATE_PATH").expect("EC_PRIVATE_PATH must be set!"))
        .expect("Unable to read ec_private file")
});
static EC_PUBLIC_KEY: Lazy<String> = Lazy::new(|| {
    fs::read_to_string(env::var("EC_PUBLIC_KEY").expect("EC_PUBLIC_KEY must be set!"))
        .expect("Unable to read ec_private file")
});
static ENCODING_KEY: Lazy<jsonwebtoken::EncodingKey> = Lazy::new(|| {
    jsonwebtoken::EncodingKey::from_ec_pem(EC_PRIVATE_KEY.as_ref())
        .expect("Fail to parse private key")
});
static DECODING_KEY: Lazy<jsonwebtoken::DecodingKey> = Lazy::new(|| {
    jsonwebtoken::DecodingKey::from_ec_pem(EC_PUBLIC_KEY.as_ref())
        .expect("Fail to parse public key")
});
static TOKEN_HEADER: Lazy<jsonwebtoken::Header> =
    Lazy::new(|| jsonwebtoken::Header::new(jsonwebtoken::Algorithm::ES384));

/// Our claims struct, it needs to derive `Serialize` and/or `Deserialize`
#[derive(Debug, Serialize, Deserialize)]
struct Claims<'a> {
    sub: &'a str,
    iss: &'a str,
    iat: i64,
    exp: i64,
}

impl<'a> Claims<'a> {
    fn new(user: &'a LoginUser) -> Self {
        let now = Utc::now().timestamp();
        Self {
            sub: user.get_username(),
            iss: ISSUER,
            exp: now + TOKEN_DURATION,
            iat: now,
        }
    }
}

#[tracing::instrument(skip(user))] // DO NOT LOG SENSITIVE DATA
pub fn generate_token(user: &LoginUser) -> String {
    debug!("Generating jwt for username {}", user.get_username());
    let claims = Claims::new(user);
    jsonwebtoken::encode(TOKEN_HEADER.deref(), &claims, ENCODING_KEY.deref()).expect(&format!(
        "Fail to encode a jwt for user {}",
        user.get_username()
    ))
}
