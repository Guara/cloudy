use crate::errors::UsersServiceResult;
use libreauth::pass::{Algorithm, HashBuilder, Hasher, LengthCalculationMethod};
use once_cell::sync::OnceCell;

static PASSWORD_HASHER: OnceCell<Hasher> = OnceCell::new();
const PWD_SCHEME_VERSION: usize = 1;
pub const FAKE_PASSWORD: &str = "$argon2$len=128,ver=2,norm=nfkc,pmin=8,lanes=4,len-calc=chars,pmax=128,xhmac=none,passes=3,mem=12$KnZb0zpt84TWoTMamCwEtA$uenpST3kAAb95TNIJ0ZJAtN7kDdPqhychetalue4J0oY2XE5qqH7dD0IUEddKjKNUleqWxGO0BOD6130Rjtc8ww1nrcD/MpV5sNiN1iBnNq2gry2BA8Xn30F7kPneYFDessjlltkd3QznDAuxD6dIyKHfesbIMc4IEVncj6+VlE";

#[tracing::instrument(skip(password))] // DO NOT LOG SENSITIVE DATA
pub fn hash_password(password: &str) -> UsersServiceResult<String> {
    let hasher = PASSWORD_HASHER.get_or_init(|| {
        HashBuilder::new()
            .length_calculation(LengthCalculationMethod::Characters)
            .min_len(8)
            .version(PWD_SCHEME_VERSION)
            .algorithm(Algorithm::Argon2)
            .finalize()
            .unwrap()
    });
    Ok(hasher.hash(password)?)
}

#[tracing::instrument(skip(stored_password, input_password))] // DO NOT LOG SENSITIVE DATA
pub fn evaluate_password(stored_password: &str, input_password: &str) -> bool {
    let checker = HashBuilder::from_phc(stored_password).unwrap();
    if checker.is_valid(input_password) {
        if checker.needs_update(Some(PWD_SCHEME_VERSION)) {
            todo!("re-hashare la password")
        }
        return true;
    }
    false
}
