use crate::entities;
use crate::errors::UsersServiceResult;
use sqlx::PgPool;

pub struct CreateUser {
    username: String,
    password: String,
}

impl CreateUser {
    /// Normalize and validate the inputs a new intermediate type
    fn new(username: String, password: String) -> UsersServiceResult<Self> {
        Ok(Self {
            username: username.trim().to_lowercase(), // TODO maybe create a newtype
            password: super::crypto_utils::hash_password(password.as_str())?,
        })
    }
}

impl Into<entities::User> for CreateUser {
    fn into(self) -> entities::User {
        entities::User::new(self.username, self.password)
    }
}

#[tracing::instrument(skip(password))] // DO NOT LOG THE PASSWORD
pub async fn handle_user_creation(
    username: String,
    password: String,
    pg_pool: &PgPool,
) -> UsersServiceResult<()> {
    let entity: entities::User = CreateUser::new(username, password)?.into();
    Ok(entity.store(pg_pool).await?)
}
