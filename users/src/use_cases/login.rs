use sqlx::PgPool;
use tracing::debug;

use crate::errors::{UsersServiceError, UsersServiceResult};
use crate::{entities, infrastructure};

pub struct LoginUser {
    username: String,
    input_password: String,
}

impl LoginUser {
    /// Normalize and validate the inputs a new intermediate type
    fn new(username: String, input_password: String) -> Self {
        Self {
            username: username.trim().to_lowercase(), // TODO maybe create a newtype
            input_password,
        }
    }

    pub fn get_username(&self) -> &str {
        self.username.as_str()
    }

    pub fn get_input_password(&self) -> &str {
        self.input_password.as_str()
    }

    #[tracing::instrument(skip(self, stored_password))] // DO NOT LOG SENSITIVE DATA
    pub fn login(self, stored_password: &str) -> Option<LoggedUser> {
        let jwt = infrastructure::token::generate_token(&self);
        if super::crypto_utils::evaluate_password(stored_password, self.get_input_password()) {
            Some(LoggedUser::new(self.username, jwt))
        } else {
            None
        }
    }
}

pub struct LoggedUser {
    username: String,
    jwt: String,
}

impl LoggedUser {
    pub fn new(username: String, jwt: String) -> Self {
        Self { username, jwt }
    }
}

#[tracing::instrument(skip(password))] // DO NOT LOG THE PASSWORD
pub async fn handle_login(
    username: String,
    password: String,
    pg_pool: &PgPool,
) -> UsersServiceResult<String> {
    debug!("Handling login for username {}", username);
    let maybe_user = LoginUser::new(username, password);
    if let Some(entity) = entities::User::get_by_username(maybe_user.get_username(), pg_pool).await
    {
        maybe_user
            .login(entity.get_password())
            .map(|logged_user| logged_user.jwt)
            .ok_or(UsersServiceError::UserNotFound)
    } else {
        let _ = maybe_user.login(super::crypto_utils::FAKE_PASSWORD);

        Err(UsersServiceError::UserNotFound)
    }
}
