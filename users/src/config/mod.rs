//! This module should provide to the application all the needed configs to run in a specific environment

use crate::errors::UsersServiceResult;
use once_cell::sync::Lazy;
use std::net::SocketAddr;

/// Users service listening IP address, could be either IPv4 or IPv6 , if IPv6 is used should be surrounded by `[]`
static USERS_SERVICE_IP_ADDRESS: Lazy<String> = Lazy::new(|| {
    std::env::var("USERS_SERVICE_IP_ADDRESS").expect("Missing env var USERS_SERVICE_IP_ADDRESS")
});
/// Users service listening port, this should be free
static USERS_SERVICE_PORT: Lazy<String> =
    Lazy::new(|| std::env::var("USERS_SERVICE_PORT").expect("Missing env var USERS_SERVICE_PORT"));
/// Parsed and validated Users service complete socket address
pub static LISTENING_ADDRESS: Lazy<SocketAddr> = Lazy::new(|| {
    let address = format!("{}:{}", &*USERS_SERVICE_IP_ADDRESS, &*USERS_SERVICE_PORT);
    address
        .parse()
        .unwrap_or_else(|_| panic!("Invalid listening address {}", address))
});

/// load .env file
pub fn load_env() -> UsersServiceResult<()> {
    dotenv::dotenv()?;
    Ok(())
}
