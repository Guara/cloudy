pub use user::*;

/// Contains all the entities handled by this service
pub mod user;
