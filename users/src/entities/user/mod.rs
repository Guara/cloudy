use sqlx::PgPool;
use uuid::Uuid;

use crate::errors::UsersServiceResult;

pub struct User {
    id: Uuid,
    username: String,
    password: String,
}

impl User {
    /// Create a new entity
    pub fn new(username: String, password: String) -> Self {
        Self {
            id: Uuid::new_v4(),
            username,
            password,
        }
    }

    pub fn get_password(&self) -> &str {
        self.password.as_str()
    }

    /// Persist on db the user
    #[tracing::instrument(skip(self))]
    pub async fn store(self, pool: &PgPool) -> UsersServiceResult<()> {
        sqlx::query!(
            r#"
                INSERT INTO users ( id, username, password )
                VALUES ( $1, $2, $3)
                RETURNING id
            "#,
            self.id,
            self.username,
            self.password
        )
        .fetch_one(pool)
        .await?;
        Ok(())
    }

    /// Search user by username
    #[tracing::instrument]
    pub async fn get_by_username(input_username: &str, pool: &PgPool) -> Option<User> {
        sqlx::query!(
            r#"
                SELECT id, username, password
                FROM users
                where username = $1
            "#,
            input_username
        )
        .fetch_one(pool)
        .await
        .ok()
        .map(|record| User {
            id: record.id,
            username: record.username,
            password: record.password,
        })
    }
}
