//! Provide all the RPC that should be exposed by this service

use sqlx::PgPool;

use crate::infrastructure::repository;

/// Provide a simple CRUD for the `User` entity
pub mod users_crud;

pub mod users_proto_types {
    tonic::include_proto!("users"); // The string specified here must match the proto package name
}

#[derive(Debug)]
pub struct UsersService {
    pg_pool: PgPool,
}

impl UsersService {
    pub fn new() -> Self {
        Self {
            pg_pool: repository::get_pool(),
        }
    }
}
