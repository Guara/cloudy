use tonic::{Request, Response, Status};

use crate::rpcs::users_proto_types::users_server::Users;
use crate::rpcs::users_proto_types::*;
use crate::rpcs::UsersService;
use crate::use_cases;

#[tonic::async_trait]
impl Users for UsersService {
    #[tracing::instrument(skip(request))]
    async fn create_user(
        &self,
        request: Request<CreateUserRequest>,
    ) -> Result<Response<CreateUserResponse>, Status> {
        let data = request.into_inner();

        use_cases::handle_user_creation(data.username, data.password, &self.pg_pool)
            .await
            .map_err(Into::<Status>::into)?;

        let reply = CreateUserResponse { done: true };

        Ok(Response::new(reply))
    }

    #[tracing::instrument(skip(request))]
    async fn login(
        &self,
        request: Request<LoginRequest>,
    ) -> Result<Response<LoginResponse>, Status> {
        let data = request.into_inner();

        let token = use_cases::handle_login(data.username, data.password, &self.pg_pool)
            .await
            .map_err(Into::<Status>::into)?;

        let reply = LoginResponse { token };

        Ok(Response::new(reply))
    }
}
