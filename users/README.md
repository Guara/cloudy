# User service

## Domain

Handler and provider of platform related users data, such as:
- Username & Password
- Roles & Permissions
- ID data
