create function set_updated_at() returns trigger
    language plpgsql
as
$$
BEGIN
    IF (
                NEW IS DISTINCT FROM OLD AND
                NEW.updated_at IS NOT DISTINCT FROM OLD.updated_at
        ) THEN
        NEW.updated_at := current_timestamp;
    END IF;
    RETURN NEW;
END;
$$;

alter function set_updated_at() owner to cloudy;

create function manage_updated_at(_tbl regclass) returns void
    language plpgsql
as
$$
BEGIN
    EXECUTE format('CREATE TRIGGER set_updated_at BEFORE UPDATE ON %s
                    FOR EACH ROW EXECUTE PROCEDURE set_updated_at()', _tbl);
END;
$$;

alter function manage_updated_at(regclass) owner to cloudy;


create TABLE users (
                       id uuid PRIMARY KEY,
                       username text UNIQUE NOT NULL,
                       password text NOT NULL,
                       registration_date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT (now() at time zone 'utc'),
                       updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT (now() at time zone 'utc'),
                       inserted_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT (now() at time zone 'utc')
    --active boolean NOT NULL DEFAULT 'f'
    --maybe add a permissions list or a reference to a permissions table
);

select manage_updated_at('users');
